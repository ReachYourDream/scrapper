import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Job {
    String title;
    String location;
    ArrayList<String> description;
    ArrayList<String> qualification;

    @SerializedName(value="posted by")
    String postedBy;

    public Job(String title, String location, ArrayList<String> descriptions, ArrayList<String> qualifications, String postedBy) {
        this.title = title;
        this.location = location;
        this.description = descriptions;
        this.qualification = qualifications;
        this.postedBy = postedBy;
    }
}
