import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileWriter;
import java.util.*;
import java.util.concurrent.*;

public class Solution {

    public static void main(String[] args)
            throws InterruptedException, ExecutionException, TimeoutException {
        System.out.println("Program is running....");
        double startTime = System.nanoTime();
        WebClient client = webClientConstruct();
        try{
            String link = "https://www.cermati.com/karir/";
            HtmlPage page = client.getPage(link);
            List<HtmlDivision> tabs = page.getByXPath("//div[contains(@class,'tab-pane')]");
            Map<String, ArrayList<Job>> jobsMapped = new HashMap<String, ArrayList<Job>>();
            Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
            ExecutorService executorService = Executors.newFixedThreadPool(10);
            Map<String, Future> tasks = new LinkedHashMap<>();
            int tabCounter = 0;
            for (HtmlDivision tab : tabs){
                Callable callable = new Callable(){
                    public Map<String, ArrayList<Job>> call() throws Exception {
                        return scrapJob(tab);
                    }
                };
                Future future = executorService.submit(callable);
                tasks.put("tab" + ++tabCounter, future);
            }
            tasks.forEach((tabNumber, future)->{
                try{
                    Map<String, ArrayList<Job>> jobScrapped = (Map<String, ArrayList<Job>>) future.get(120,TimeUnit.SECONDS);
                    jobsMapped.putAll(jobScrapped);
                } catch(InterruptedException | ExecutionException | TimeoutException e){
                    e.printStackTrace();
                    System.out.println("error");
                }
            });
            executorService.shutdown();
            FileWriter writer = new FileWriter(new File("solution.json"));
            String jsonFinal = gson.toJson(jobsMapped);
            writer.write(jsonFinal);
            writer.flush();
            writer.close();
        } catch(Exception e){
            e.printStackTrace();
        }
        double processingTime = ( System.nanoTime()- startTime)/1000000000;
        System.out.println("Program has done running in " + processingTime  + " seconds");
    }

    private static WebClient webClientConstruct(){
        WebClient client = new WebClient();
        client.getOptions().setCssEnabled(false);
        client.getOptions().setJavaScriptEnabled(false);
        return client;
    }

    private static Map<String, ArrayList<Job>> scrapJob(HtmlDivision tab){
        List<HtmlDivision> divs = tab.getByXPath(".//div");
        String departmentLabel = divs.get(0).asText();
        DomElement tabContainer =  tab.getLastElementChild();
        Iterable<DomElement> tabContainerList = tabContainer.getChildElements();
        ArrayList<Job> jobs = new ArrayList<>();
        Map<String, ArrayList<Job>> jobsMapped = new HashMap<>();
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        Map<String, Future> tasks = new LinkedHashMap<>();
        int childCounter = 0;
        for (DomElement jobList : tabContainerList){
            Callable callable = new Callable(){
                public Job call() throws Exception {
                    return scrapChild(jobList);
                }
            };
            Future future = executorService.submit(callable);
            tasks.put("Child" + ++childCounter,future);
        }
        tasks.forEach((childNumber,future)->{
            try{
                Job childJob = (Job) future.get(120,TimeUnit.SECONDS);
                jobs.add(childJob);
            } catch(InterruptedException | ExecutionException | TimeoutException e){
                e.printStackTrace();
                System.out.println("error");
            }
        });
        executorService.shutdown();
        jobsMapped.put(departmentLabel,jobs);
        return jobsMapped;
    }

    private static Job scrapChild(DomElement jobList){
        WebClient client = webClientConstruct();
        DomElement jobDetail = jobList.getFirstElementChild();
        String jobDetailLink =  jobDetail.getAttribute("href");
        HtmlPage smartRecruiter = null;
        try{
            smartRecruiter = client.getPage(jobDetailLink);
        } catch (Exception e){
            e.printStackTrace();
        }
        //Job Title
        List<HtmlMeta> titleXML = smartRecruiter.getByXPath("//meta[contains(@property, 'og:title')]");
        String title ="";
        for (HtmlMeta titles :  titleXML){
            title = titles.getAttribute("content");
        }
        //Job Address
        List<HtmlSpan> addressXML = smartRecruiter.getByXPath("//span[contains(@itemprop, 'address')]");
        String address = "";
        for (HtmlSpan addresses :  addressXML){
            address = addresses.asText();
        }
        //Job Qualification
        ArrayList<String> qualificationList = new ArrayList<String>();
        List<HtmlDivision> qualificationsXML = smartRecruiter.getByXPath("//div[contains(@itemprop,'qualifications')]");
        for (HtmlDivision qualifications : qualificationsXML ){
            DomNodeList<HtmlElement> qualificationUlList = qualifications.getElementsByTagName("ul");
            for (HtmlElement qualificationUl : qualificationUlList){
                Iterable<DomElement> qualificationLi =  qualificationUl.getChildElements();
                for (DomElement qualification : qualificationLi){
                    qualificationList.add(qualification.getFirstChild().asText());
                }
            }
        }
        //Job Description
        ArrayList<String> descriptionList = new ArrayList<String>();
        List<HtmlDivision> descriptionsXML = smartRecruiter.getByXPath("//div[contains(@itemprop,'responsibilities')]");
        for (HtmlDivision descriptions : descriptionsXML ){
            DomNodeList<HtmlElement> descriptionUlList = descriptions.getElementsByTagName("ul");
            for (HtmlElement descriptionUl : descriptionUlList){
                Iterable<DomElement> descriptionLi =  descriptionUl.getChildElements();
                for (DomElement description : descriptionLi){
                    descriptionList.add(description.getFirstChild().asText());
                }
            }
        }
        //Job Poster
        String jobPosterName = "";
        List<HtmlDivision> jobPosterContainer = smartRecruiter.getByXPath("//div[contains(@class,'media media--aligned')]");
        for(HtmlDivision jobPosterContentContainer :  jobPosterContainer) {
            DomElement jobPosterMediaContentContainer = jobPosterContentContainer.getLastElementChild();
            jobPosterName = jobPosterMediaContentContainer.getLastElementChild().getLastElementChild().asText();
        }
        Job job = new Job(title, address, descriptionList, qualificationList, jobPosterName);
        return job;
    }
}
